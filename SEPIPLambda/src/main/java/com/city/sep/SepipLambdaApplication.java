package com.city.sep;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

import com.city.sep.controller.SepipController;

@SpringBootApplication
@Import({ SepipController.class })
public class SepipLambdaApplication extends SpringBootServletInitializer  {
	
	// silence console logging
    @Value("${logging.level.root:OFF}")
    String message = "";

	public static void main(String[] args) {
		SpringApplication.run(SepipLambdaApplication.class, args);
	}

}
