/**
 * 
 */
package com.city.sep.controller;


import java.util.Arrays;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.city.sep.model.CustomerHubModel;
import com.city.sep.model.SepipDB;
import com.city.sep.model.SepipModel;
import com.city.sep.repo.SepipRepo;
import com.city.sep.util.Constants;
import com.city.sep.util.SepipFieldValidationUtil;
import com.city.sep.util.ValidationUtil;

/**
 * @author SU20053232
 *
 */

@RestController
@RequestMapping("/v1")
public class SepipController {
	
	/*@Autowired
	private RestTemplate restTemplate;*/
	
	/*@Autowired
	private SepipRepo sepipRepo;	*/
	
	@RequestMapping(value="/sepip", method = RequestMethod.POST)
	public String getSepipValues(@Valid @RequestBody SepipModel sepipModel) {
		
		System.out.println("Start SEPIP Service");
		SepipDB sepipDb = null;
		CustomerHubModel customerHubModel = null;
		String validatationResult = null;
		CustomerHubModel model = new CustomerHubModel();
		
		validatationResult = ValidationUtil.validateInputFields(sepipModel); 
		
		System.out.println("validatationResult : " + validatationResult);
	    
	    //boolean validateInputFields = SepipFieldValidationUtil.validateInputFields(sepipModel);
	    //System.out.println(validateInputFields);
	    
	    if (validatationResult != null) {	
	    	return validatationResult;
	    } else {	    	
	    	sepipDb = new SepipDB();			
	    	customerHubModel = new CustomerHubModel();		
			
			customerHubModel.setTrackingKey(sepipModel.getTrackingKey());
			customerHubModel.setPaymentType(sepipModel.getPaymentType());
			customerHubModel.setAmountOfPayment(sepipModel.getAmountOfPayment());
			customerHubModel.setBeneficiaryAccountType2(sepipModel.getBeneficiaryAccountType2());
			
			customerHubModel.setCalenderDatePayment(sepipModel.getCalenderDatePayment());
			customerHubModel.setCalenderTimePayment(sepipModel.getCalenderTimePayment());
			customerHubModel.setCdaParticipientSPEIKey(sepipModel.getCdaParticipientSPEIKey());
			customerHubModel.setConceptOfPayment(sepipModel.getConceptOfPayment());
			customerHubModel.setNameOfTransferOrderIssuerParticipant(sepipModel.getNameOfTransferOrderIssuerParticipant());
			customerHubModel.setOrderingName(sepipModel.getOrderingName());
			customerHubModel.setOrdererAccountType(sepipModel.getOrdererAccountType());
			customerHubModel.setOrderingAccount(sepipModel.getOrderingAccount());
			customerHubModel.setRfcOrCurpPayer(sepipModel.getRfcOrCurpPayer());
			customerHubModel.setTypeOfBeneficiaryAccount(sepipModel.getTypeOfBeneficiaryAccount());			
			customerHubModel.setConceptOfPayment(sepipModel.getConceptOfPayment());
			customerHubModel.setIvaAmount(sepipModel.getIvaAmount());
			customerHubModel.setAmountOfPayment(sepipModel.getAmountOfPayment());
			customerHubModel.setBeneficiaryAccountType2(sepipModel.getBeneficiaryAccountType2());
			customerHubModel.setDigitalCollectionSchemeFolio(sepipModel.getDigitalCollectionSchemeFolio());
			customerHubModel.setPaymentOfTheTransferFee(sepipModel.getPaymentOfTheTransferFee());
			customerHubModel.setTransferFeeAmount(sepipModel.getTransferFeeAmount());
			customerHubModel.setBuyerCellNumberAliases(sepipModel.getBuyerCellNumberAliases());
			customerHubModel.setDigitBuyersVerifier(sepipModel.getDigitBuyersVerifier());
			customerHubModel.setSpeiOperationDate(sepipModel.getSpeiOperationDate());
			
					
			/*HttpHeaders headers = new HttpHeaders();	    
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		    HttpEntity<CustomerHubModel> entity = new  HttpEntity<CustomerHubModel>(customerHubModel, headers);	*/
		    
		    //Get the value from External System	    
		     //restTemplate.postForObject(Constants.CUSTOMER_HUB_URL, entity, CustomerHubModel.class);   
		   		
			
			//Input Fields
			model.setTrackingKey(customerHubModel.getTrackingKey());
			model.setPaymentType(customerHubModel.getPaymentType());
			model.setAmountOfPayment(customerHubModel.getAmountOfPayment());
			model.setBeneficiaryAccountType2(customerHubModel.getBeneficiaryAccountType2());
			model.setBuyerCellNumberAliases(customerHubModel.getBuyerCellNumberAliases());
			model.setCalenderDatePayment(customerHubModel.getCalenderDatePayment());
			model.setCalenderTimePayment(customerHubModel.getCalenderTimePayment());
			model.setCdaParticipientSPEIKey(customerHubModel.getCdaParticipientSPEIKey());
			model.setConceptOfPayment(customerHubModel.getConceptOfPayment());
			model.setNameOfTransferOrderIssuerParticipant(customerHubModel.getNameOfTransferOrderIssuerParticipant());
			model.setOrderingName(customerHubModel.getOrderingName());
			model.setOrdererAccountType(customerHubModel.getOrdererAccountType());
			model.setOrderingAccount(customerHubModel.getOrderingAccount());
			model.setRfcOrCurpPayer(customerHubModel.getRfcOrCurpPayer());
			model.setTypeOfBeneficiaryAccount(customerHubModel.getTypeOfBeneficiaryAccount());
			model.setConceptOfPayment(customerHubModel.getConceptOfPayment());
			model.setIvaAmount(customerHubModel.getIvaAmount());
			model.setAmountOfPayment(customerHubModel.getAmountOfPayment());
			model.setBeneficiaryAccountType2(customerHubModel.getBeneficiaryAccountType2());
			model.setDigitalCollectionSchemeFolio(customerHubModel.getDigitalCollectionSchemeFolio());
			model.setPaymentOfTheTransferFee(customerHubModel.getPaymentOfTheTransferFee());
			model.setTransferFeeAmount(customerHubModel.getTransferFeeAmount());
			model.setBuyerCellNumberAliases(customerHubModel.getBuyerCellNumberAliases());
			model.setDigitBuyersVerifier(customerHubModel.getDigitBuyersVerifier());
			model.setSpeiOperationDate(customerHubModel.getSpeiOperationDate());
			
			
			//Output Fields
			model.setMombreDelParticipanteReceptor("MombreDelParticipanteReceptor");
			model.setNombreBeneficiario("NombreBeneficiario");		
			model.setCuentaBeneficiario(123);
			model.setrFCOCurpBeneficiario("rFCOCurpBeneficiario");
			model.setNombreBeneficiario2("NombreBeneficiario2");
			model.setrFCOCurpBeneficiario2("setrFCOCurpBeneficiario2");	
			model.setCuentaBeneficiario2(1234556);
			model.setAliasDelNumeroCelularDelVendedor(234);
			model.setDigitoVerificadorDelVendedor(234);
			model.setNumeroDeSerieDelCertificado("numeroDeSerieDelCertificado");
			model.setSelloDigital("12346678899763qeekjhdiqwudhiasnxkanhduiqwaxnKDJNiqwuhI");
			
			sepipDb.setTrackingKey(model.getTrackingKey());
			sepipDb.setCustomerHubModel(model);
		
			//Store the value got from External System to Mongo DB
			//sepipRepo.save(sepipDb);
			
			
			return "||" + model.getPaymentType() + "|" + model.getAmountOfPayment() + "|" + model.getBeneficiaryAccountType2() + "|" + model.getBuyerCellNumberAliases()
			           + "|" + model.getCalenderDatePayment() + "|" + model.getCalenderTimePayment() + "|" + model.getCdaParticipientSPEIKey() + "|" + model.getConceptOfPayment()
			           + "|" + model.getNameOfTransferOrderIssuerParticipant() + "|" + model.getOrderingName() + "|"  + model.getOrdererAccountType() + "|" + model.getOrderingAccount()
			           + "|" + model.getRfcOrCurpPayer() + "|" + model.getTypeOfBeneficiaryAccount() + "|" + model.getConceptOfPayment() + "|" + model.getIvaAmount() + "|" + model.getAmountOfPayment()
			           + "|" + model.getBeneficiaryAccountType2() + "|" + model.getDigitalCollectionSchemeFolio() + "|" + model.getPaymentOfTheTransferFee() + "|" + model.getTransferFeeAmount()
			           + "|" + model.getBuyerCellNumberAliases() + "|" + model.getDigitBuyersVerifier() + "|" + model.getDigitBuyersVerifier() + "|" + model.getSpeiOperationDate()
			           + "|" + model.getMombreDelParticipanteReceptor() + "|" + model.getNombreBeneficiario() + "|" + model.getCuentaBeneficiario() + model.getrFCOCurpBeneficiario()
			           + "|" + model.getNombreBeneficiario2() + "|" + model.getrFCOCurpBeneficiario2() + "|" + model.getCuentaBeneficiario2() + "|" + model.getAliasDelNumeroCelularDelVendedor()
			           + "|" + model.getDigitoVerificadorDelVendedor() + "|" + model.getNumeroDeSerieDelCertificado() + "|" + model.getSelloDigital() + "||";
	    	
	    }
		
	}

}
