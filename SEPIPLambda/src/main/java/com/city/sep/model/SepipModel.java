/**
 *
 */
package com.city.sep.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author SU20053232
 *
 */
public class SepipModel {

	@NotNull(message="Clave de Rastreo No puede estar vacío")
	@Size(min=1, max=30, message="Clave de Rastreo Entre 1 y 30 caracteres.")
	@JsonProperty("Clave de Rastreo")
	public String trackingKey;

	@JsonProperty("Tipo de pago")
	@NotNull(message="Tipo de pago No puede estar vacío")
	/*@Min(1)
	@Max(99)*/
	public Integer paymentType;

	@JsonProperty("Fecha de operación del SPEI")
	@NotNull(message="Fecha de operación del SPEI No puede estar vacío")	
	/*@Min(1)
	@Max(99999999)*/
	public Integer speiOperationDate;

	@JsonProperty("Fecha calendario del abono")
	@NotNull(message="Fecha calendario del abono No puede estar vacío")	
	/*@Min(1)
	@Max(99999999)*/
	public Integer calenderDatePayment;

	@JsonProperty("Hora calendario del abono")
	@NotNull(message="Hora calendario del abono No puede estar vacío")
	public String calenderTimePayment;

	@JsonProperty("Clave SPEI del Participante Emisor de la CDA")
	@NotNull(message="Clave SPEI del Participante Emisor de la CDA No puede estar vacío")
	public Integer cdaParticipientSPEIKey;

	@JsonProperty("Nombre del Participante Emisor de la Orden de Transferencia")
	@NotNull(message="Nombre del Participante Emisor de la Orden de Transferencia No puede estar vacío")
	public String nameOfTransferOrderIssuerParticipant;

	@JsonProperty("Nombre Ordenante")
	@NotNull(message="Nombre Ordenante No puede estar vacío")
	//@Size(min=1, max=40, message="Valor inválido, Nombre de Beneficiario no puede ser mayor a 40 posiciones")
	public String orderingName;

	@JsonProperty("Tipo de Cuenta Ordenante")
	@NotNull(message="Tipo de Cuenta Ordenante No puede estar vacío")	
	/*@Min(1)
	@Max(99)*/
	public Integer ordererAccountType;

	@JsonProperty("Cuenta Ordenante")
	@NotNull(message="Cuenta Ordenante No puede estar vacío")
	@Min(1)
	//@Max(20)
	public long orderingAccount;

	@JsonProperty("RFC o CURP Ordenante")
	@NotNull(message="RFC o CURP Ordenante No puede estar vacío")
	@Size(min=1, max=18, message="Valor inválido, RFC o CURP Beneficiario 2 no puede ser mayor a 18 posiciones")
	public String rfcOrCurpPayer;

	@JsonProperty("Tipo de Cuenta Beneficiario")
	@NotNull(message="Tipo de Cuenta Beneficiario No puede estar vacío")
	public Integer typeOfBeneficiaryAccount;

	@JsonProperty("Concepto del Pago")
	@NotNull(message="Concepto del Pago No puede estar vacío")
	public String conceptOfPayment;

	@JsonProperty("Importe del IVA")
	@NotNull(message="Importe del IVA No puede estar vacío")
	@Min(1)
	//@Max(19)
	public long ivaAmount;

	@JsonProperty("Monto del pago")
	@NotNull(message="Monto del pago No puede estar vacío")
	public long amountOfPayment;

	@JsonProperty("Tipo de Cuenta Beneficiario 2")
	@NotNull(message="Tipo de Cuenta Beneficiario 2 No puede estar vacío")
	public Integer BeneficiaryAccountType2;

	@JsonProperty("Folio del Esquema Cobro Digital")
	@NotNull(message="Folio del Esquema Cobro Digital No puede estar vacío")
	@Size(min=1, max=20, message="Valor inválido,Folio del Esquema Cobro Digital, no puede ser mayor a 20 posiciones")
	public String digitalCollectionSchemeFolio;

	@JsonProperty("Pago de la comisión por la transferencia")
	@NotNull(message="Pago de la comisión por la transferencia No puede estar vacío")
	public Integer paymentOfTheTransferFee;

	@JsonProperty("Monto de la comisión por la transferencia")
	@NotNull(message="Monto de la comisión por la transferencia No puede estar vacío")
	public long transferFeeAmount;

	@JsonProperty("Alias del número celular del comprador")
	@NotNull(message="Alias del número celular del comprador No puede estar vacío")
	public String buyerCellNumberAliases;

	@JsonProperty("Digito verificador del comprador")
	@NotNull(message="Digito verificador del comprador No puede estar vacío")
	public String  digitBuyersVerifier;

	public String getTrackingKey() {
		return trackingKey;
	}

	public void setTrackingKey(String trackingKey) {
		this.trackingKey = trackingKey;
	}

	public int getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}

	public int getSpeiOperationDate() {
		return speiOperationDate;
	}

	public void setSpeiOperationDate(int speiOperationDate) {
		this.speiOperationDate = speiOperationDate;
	}

	public int getCalenderDatePayment() {
		return calenderDatePayment;
	}

	public void setCalenderDatePayment(int calenderDatePayment) {
		this.calenderDatePayment = calenderDatePayment;
	}

	public String getCalenderTimePayment() {
		return calenderTimePayment;
	}

	public void setCalenderTimePayment(String calenderTimePayment) {
		this.calenderTimePayment = calenderTimePayment;
	}

	public int getCdaParticipientSPEIKey() {
		return cdaParticipientSPEIKey;
	}

	public void setCdaParticipientSPEIKey(int cdaParticipientSPEIKey) {
		this.cdaParticipientSPEIKey = cdaParticipientSPEIKey;
	}

	public String getNameOfTransferOrderIssuerParticipant() {
		return nameOfTransferOrderIssuerParticipant;
	}

	public void setNameOfTransferOrderIssuerParticipant(String nameOfTransferOrderIssuerParticipant) {
		this.nameOfTransferOrderIssuerParticipant = nameOfTransferOrderIssuerParticipant;
	}

	public String getOrderingName() {
		return orderingName;
	}

	public void setOrderingName(String orderingName) {
		this.orderingName = orderingName;
	}

	public int getOrdererAccountType() {
		return ordererAccountType;
	}

	public void setOrdererAccountType(int ordererAccountType) {
		this.ordererAccountType = ordererAccountType;
	}

	public long getOrderingAccount() {
		return orderingAccount;
	}

	public void setOrderingAccount(long orderingAccount) {
		this.orderingAccount = orderingAccount;
	}

	public String getRfcOrCurpPayer() {
		return rfcOrCurpPayer;
	}

	public void setRfcOrCurpPayer(String rfcOrCurpPayer) {
		this.rfcOrCurpPayer = rfcOrCurpPayer;
	}

	public int getTypeOfBeneficiaryAccount() {
		return typeOfBeneficiaryAccount;
	}

	public void setTypeOfBeneficiaryAccount(int typeOfBeneficiaryAccount) {
		this.typeOfBeneficiaryAccount = typeOfBeneficiaryAccount;
	}

	public String getConceptOfPayment() {
		return conceptOfPayment;
	}

	public void setConceptOfPayment(String conceptOfPayment) {
		this.conceptOfPayment = conceptOfPayment;
	}

	public long getIvaAmount() {
		return ivaAmount;
	}

	public void setIvaAmount(long ivaAmount) {
		this.ivaAmount = ivaAmount;
	}

	public long getAmountOfPayment() {
		return amountOfPayment;
	}

	public void setAmountOfPayment(long amountOfPayment) {
		this.amountOfPayment = amountOfPayment;
	}

	public int getBeneficiaryAccountType2() {
		return BeneficiaryAccountType2;
	}

	public void setBeneficiaryAccountType2(int beneficiaryAccountType2) {
		BeneficiaryAccountType2 = beneficiaryAccountType2;
	}

	public String getDigitalCollectionSchemeFolio() {
		return digitalCollectionSchemeFolio;
	}

	public void setDigitalCollectionSchemeFolio(String digitalCollectionSchemeFolio) {
		this.digitalCollectionSchemeFolio = digitalCollectionSchemeFolio;
	}

	public int getPaymentOfTheTransferFee() {
		return paymentOfTheTransferFee;
	}

	public void setPaymentOfTheTransferFee(int paymentOfTheTransferFee) {
		this.paymentOfTheTransferFee = paymentOfTheTransferFee;
	}

	public long getTransferFeeAmount() {
		return transferFeeAmount;
	}

	public void setTransferFeeAmount(long transferFeeAmount) {
		this.transferFeeAmount = transferFeeAmount;
	}

	public String getBuyerCellNumberAliases() {
		return buyerCellNumberAliases;
	}

	public void setBuyerCellNumberAliases(String buyerCellNumberAliases) {
		this.buyerCellNumberAliases = buyerCellNumberAliases;
	}

	public String getDigitBuyersVerifier() {
		return digitBuyersVerifier;
	}

	public void setDigitBuyersVerifier(String digitBuyersVerifier) {
		this.digitBuyersVerifier = digitBuyersVerifier;
	}

}
