/**
 * 
 */
package com.city.sep.model;

//import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author SU20053232
 *
 */
public class CustomerHubModel {
	
	//@Id
	@JsonProperty("Clave de Rastreo")
	public String trackingKey;
	
	@JsonProperty("Tipo de pago")
	public Integer paymentType; 
	
	@JsonProperty("Fecha de operación del SPEI")    
	public Integer speiOperationDate;
	
	@JsonProperty("Fecha calendario del abono")
	public Integer calenderDatePayment;
	
	@JsonProperty("Hora calendario del abono")
	public String calenderTimePayment;
	
	@JsonProperty("Clave SPEI del Participante Emisor de la CDA")
	public Integer cdaParticipientSPEIKey;
	
	@JsonProperty("Nombre del Participante Emisor de la Orden de Transferencia")
	public String nameOfTransferOrderIssuerParticipant;
	
	@JsonProperty("Nombre Ordenante")
	public String orderingName;
	
	@JsonProperty("Tipo de Cuenta Ordenante")
	public Integer ordererAccountType;
	
	@JsonProperty("Cuenta Ordenante")
	public long orderingAccount;
	
	@JsonProperty("RFC o CURP Ordenante")
	public String rfcOrCurpPayer;
	
	@JsonProperty("Tipo de Cuenta Beneficiario")
	public Integer typeOfBeneficiaryAccount;
	
	@JsonProperty("Concepto del Pago")
	public String conceptOfPayment;
	
	@JsonProperty("Importe del IVA")
	public long ivaAmount;
	
	@JsonProperty("Monto del pago")
	public long amountOfPayment;
	
	@JsonProperty("Tipo de Cuenta Beneficiario 2")
	public Integer BeneficiaryAccountType2;
	
	@JsonProperty("Folio del Esquema Cobro Digital")
	public String digitalCollectionSchemeFolio;
	
	@JsonProperty("Pago de la comisión por la transferencia")
	public Integer paymentOfTheTransferFee;
	
	@JsonProperty("Monto de la comisión por la transferencia")
	public long transferFeeAmount;
	
	@JsonProperty("Alias del número celular del comprador")
	public String buyerCellNumberAliases;
	
	@JsonProperty("Digito verificador del comprador")
	public String  digitBuyersVerifier;
	
	public String mombreDelParticipanteReceptor;
	
	public String nombreBeneficiario;
	
	public int cuentaBeneficiario;
	
	public String rFCOCurpBeneficiario;
	
	public String nombreBeneficiario2;
	
	public String rFCOCurpBeneficiario2;
	
	public int cuentaBeneficiario2;
	
	public int aliasDelNumeroCelularDelVendedor;
	
	public int digitoVerificadorDelVendedor;
	
	public String numeroDeSerieDelCertificado;
	
	public String selloDigital;
	

	public String getTrackingKey() {
		return trackingKey;
	}

	public void setTrackingKey(String trackingKey) {
		this.trackingKey = trackingKey;
	}

	public int getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}

	public int getSpeiOperationDate() {
		return speiOperationDate;
	}

	public void setSpeiOperationDate(int speiOperationDate) {
		this.speiOperationDate = speiOperationDate;
	}

	public int getCalenderDatePayment() {
		return calenderDatePayment;
	}

	public void setCalenderDatePayment(int calenderDatePayment) {
		this.calenderDatePayment = calenderDatePayment;
	}

	public String getCalenderTimePayment() {
		return calenderTimePayment;
	}

	public void setCalenderTimePayment(String calenderTimePayment) {
		this.calenderTimePayment = calenderTimePayment;
	}

	public int getCdaParticipientSPEIKey() {
		return cdaParticipientSPEIKey;
	}

	public void setCdaParticipientSPEIKey(int cdaParticipientSPEIKey) {
		this.cdaParticipientSPEIKey = cdaParticipientSPEIKey;
	}

	public String getNameOfTransferOrderIssuerParticipant() {
		return nameOfTransferOrderIssuerParticipant;
	}

	public void setNameOfTransferOrderIssuerParticipant(String nameOfTransferOrderIssuerParticipant) {
		this.nameOfTransferOrderIssuerParticipant = nameOfTransferOrderIssuerParticipant;
	}

	public String getOrderingName() {
		return orderingName;
	}

	public void setOrderingName(String orderingName) {
		this.orderingName = orderingName;
	}

	public int getOrdererAccountType() {
		return ordererAccountType;
	}

	public void setOrdererAccountType(int ordererAccountType) {
		this.ordererAccountType = ordererAccountType;
	}

	public long getOrderingAccount() {
		return orderingAccount;
	}

	public void setOrderingAccount(long orderingAccount) {
		this.orderingAccount = orderingAccount;
	}

	public String getRfcOrCurpPayer() {
		return rfcOrCurpPayer;
	}

	public void setRfcOrCurpPayer(String rfcOrCurpPayer) {
		this.rfcOrCurpPayer = rfcOrCurpPayer;
	}

	public int getTypeOfBeneficiaryAccount() {
		return typeOfBeneficiaryAccount;
	}

	public void setTypeOfBeneficiaryAccount(int typeOfBeneficiaryAccount) {
		this.typeOfBeneficiaryAccount = typeOfBeneficiaryAccount;
	}

	public String getConceptOfPayment() {
		return conceptOfPayment;
	}

	public void setConceptOfPayment(String conceptOfPayment) {
		this.conceptOfPayment = conceptOfPayment;
	}

	public long getIvaAmount() {
		return ivaAmount;
	}

	public void setIvaAmount(long ivaAmount) {
		this.ivaAmount = ivaAmount;
	}

	public long getAmountOfPayment() {
		return amountOfPayment;
	}

	public void setAmountOfPayment(long amountOfPayment) {
		this.amountOfPayment = amountOfPayment;
	}

	public int getBeneficiaryAccountType2() {
		return BeneficiaryAccountType2;
	}

	public void setBeneficiaryAccountType2(int beneficiaryAccountType2) {
		BeneficiaryAccountType2 = beneficiaryAccountType2;
	}

	public String getDigitalCollectionSchemeFolio() {
		return digitalCollectionSchemeFolio;
	}

	public void setDigitalCollectionSchemeFolio(String digitalCollectionSchemeFolio) {
		this.digitalCollectionSchemeFolio = digitalCollectionSchemeFolio;
	}

	public int getPaymentOfTheTransferFee() {
		return paymentOfTheTransferFee;
	}

	public void setPaymentOfTheTransferFee(int paymentOfTheTransferFee) {
		this.paymentOfTheTransferFee = paymentOfTheTransferFee;
	}

	public long getTransferFeeAmount() {
		return transferFeeAmount;
	}

	public void setTransferFeeAmount(long transferFeeAmount) {
		this.transferFeeAmount = transferFeeAmount;
	}

	public String getBuyerCellNumberAliases() {
		return buyerCellNumberAliases;
	}

	public void setBuyerCellNumberAliases(String buyerCellNumberAliases) {
		this.buyerCellNumberAliases = buyerCellNumberAliases;
	}

	public String getDigitBuyersVerifier() {
		return digitBuyersVerifier;
	}

	public void setDigitBuyersVerifier(String digitBuyersVerifier) {
		this.digitBuyersVerifier = digitBuyersVerifier;
	}

	public String getMombreDelParticipanteReceptor() {
		return mombreDelParticipanteReceptor;
	}

	public void setMombreDelParticipanteReceptor(String mombreDelParticipanteReceptor) {
		this.mombreDelParticipanteReceptor = mombreDelParticipanteReceptor;
	}

	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}

	public int getCuentaBeneficiario() {
		return cuentaBeneficiario;
	}

	public void setCuentaBeneficiario(int cuentaBeneficiario) {
		this.cuentaBeneficiario = cuentaBeneficiario;
	}

	public String getrFCOCurpBeneficiario() {
		return rFCOCurpBeneficiario;
	}

	public void setrFCOCurpBeneficiario(String rFCOCurpBeneficiario) {
		this.rFCOCurpBeneficiario = rFCOCurpBeneficiario;
	}

	public String getNombreBeneficiario2() {
		return nombreBeneficiario2;
	}

	public void setNombreBeneficiario2(String nombreBeneficiario2) {
		this.nombreBeneficiario2 = nombreBeneficiario2;
	}

	public String getrFCOCurpBeneficiario2() {
		return rFCOCurpBeneficiario2;
	}

	public void setrFCOCurpBeneficiario2(String rFCOCurpBeneficiario2) {
		this.rFCOCurpBeneficiario2 = rFCOCurpBeneficiario2;
	}

	public int getCuentaBeneficiario2() {
		return cuentaBeneficiario2;
	}

	public void setCuentaBeneficiario2(int cuentaBeneficiario2) {
		this.cuentaBeneficiario2 = cuentaBeneficiario2;
	}

	public int getAliasDelNumeroCelularDelVendedor() {
		return aliasDelNumeroCelularDelVendedor;
	}

	public void setAliasDelNumeroCelularDelVendedor(int aliasDelNumeroCelularDelVendedor) {
		this.aliasDelNumeroCelularDelVendedor = aliasDelNumeroCelularDelVendedor;
	}

	public int getDigitoVerificadorDelVendedor() {
		return digitoVerificadorDelVendedor;
	}

	public void setDigitoVerificadorDelVendedor(int digitoVerificadorDelVendedor) {
		this.digitoVerificadorDelVendedor = digitoVerificadorDelVendedor;
	}

	public String getNumeroDeSerieDelCertificado() {
		return numeroDeSerieDelCertificado;
	}

	public void setNumeroDeSerieDelCertificado(String numeroDeSerieDelCertificado) {
		this.numeroDeSerieDelCertificado = numeroDeSerieDelCertificado;
	}

	public String getSelloDigital() {
		return selloDigital;
	}

	public void setSelloDigital(String selloDigital) {
		this.selloDigital = selloDigital;
	}

}
