package com.city.sep.lambda;


import com.amazonaws.serverless.exceptions.ContainerInitializationException;
import com.amazonaws.serverless.proxy.internal.testutils.Timer;
import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import com.amazonaws.serverless.proxy.model.AwsProxyResponse;
import com.amazonaws.serverless.proxy.spring.SpringBootLambdaContainerHandler;
import com.amazonaws.serverless.proxy.spring.SpringBootProxyHandlerBuilder;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.city.sep.SepipLambdaApplication;




/**
 * @author SU20053232
 *
 */
public class StreamLambdaHandler implements RequestStreamHandler {
	private static Logger log = LoggerFactory.getLogger(StreamLambdaHandler.class);
	private static SpringBootLambdaContainerHandler<AwsProxyRequest, AwsProxyResponse> handler;
	
	static {
		try {
			log.info("Starting AWS Lambda");
			handler = SpringBootLambdaContainerHandler.getAwsProxyHandler(SepipLambdaApplication.class);
			/*handler.onStartup(servletContext -> {
                FilterRegistration.Dynamic registration = servletContext.addFilter("CognitoIdentityFilter", CognitoIdentityFilter.class);
                registration.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
            });*/
			
			/*handler = new SpringBootProxyHandlerBuilder()	            
	                .springBootApplication(SepipLambdaApplication.class)
	                .buildAndInitialize();*/
			log.info("Created AWS Handler, {}", handler);			
		} catch (ContainerInitializationException e) {
            // if we fail here. We re-throw the exception to force another cold start
            //e.printStackTrace();
            throw new RuntimeException("Not able to Initialize the Application : ", e);
        }
	}
	
	public StreamLambdaHandler() {
        // we enable the timer for debugging. This SHOULD NOT be enabled in production.
        Timer.enable();
    }
	
	@Override
	@SuppressWarnings("unchecked")
	public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {
		
		log.info("Handle request {}, {}, {}, {}", context, context.getFunctionName(), context.getInvokedFunctionArn(),
                context.getLogStreamName());
		handler.proxyStream(inputStream, outputStream, context);
        
	}
	

}
