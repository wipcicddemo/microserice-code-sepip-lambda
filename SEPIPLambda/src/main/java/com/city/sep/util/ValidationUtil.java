/**
 *
 */
package com.city.sep.util;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import com.city.sep.model.SepipModel;

/**
 * @author SU20053232
 *
 */
public class ValidationUtil {

	public static String validateInputFields(SepipModel sepipModel) {

		String validationResult = ValidationUtil.validateAllPaymentTypeInfo(sepipModel.getPaymentType());

		System.out.println("validationResult after PAyment Type" + validationResult);

		String validationResultSpeiOperationDate = ValidationUtil
				.validateAllSpeiOperationDate(sepipModel.getSpeiOperationDate());

		System.out.println("validationResultSpeiOperationDate" + validationResultSpeiOperationDate);
		System.out.println("validationResultSpeiOperationDate in Model" + validationResult);
		if (validationResultSpeiOperationDate != null) {
			validationResult += " ," + validationResultSpeiOperationDate;
			System.out.println(validationResult + "In if validationResultSpeiOperationDate");
		} /*
			 * else { validationResult = validationResultSpeiOperationDate;
			 * System.out.println(validationResult +
			 * "In else validationResultSpeiOperationDate"); }
			 */

		/*
		 * String validateDigitBuyersVerifierStr = ValidationUtil
		 * .validateDigitBuyersVerifier(sepipModel.getDigitBuyersVerifier()); if
		 * (validationResult != null) { validationResult += " ," +
		 * validateDigitBuyersVerifierStr; } else { validationResult =
		 * validateDigitBuyersVerifierStr; }
		 * 
		 * String validateBuyerCellNumberAliasesStr = ValidationUtil
		 * .validateBuyerCellNumberAliases(sepipModel.getBuyerCellNumberAliases()); if
		 * (validationResult != null) { validationResult += " ," +
		 * validateBuyerCellNumberAliasesStr; } else { validationResult =
		 * validateBuyerCellNumberAliasesStr; }
		 */

		String validatePaymentOfTheTransferFeeStr = ValidationUtil
				.validatePaymentOfTheTransferFee(sepipModel.getPaymentOfTheTransferFee());
		if (validatePaymentOfTheTransferFeeStr != null) {
			validationResult += " ," + validatePaymentOfTheTransferFeeStr;
		} /*
			 * else { validationResult = validatePaymentOfTheTransferFeeStr; }
			 */

		String validateBeneficiaryAccountType2Str = ValidationUtil
				.validateBeneficiaryAccountType2(sepipModel.getBeneficiaryAccountType2());
		if (validateBeneficiaryAccountType2Str != null) {
			validationResult += " ," + validateBeneficiaryAccountType2Str;
		}

		String validateCalenderTimePaymentStr = ValidationUtil
				.validateCalenderTimePayment(sepipModel.getCalenderTimePayment());
		if (validateCalenderTimePaymentStr != null) {
			validationResult += " ," + validateCalenderTimePaymentStr;
		}

		String validateOrderingAccountStr = ValidationUtil.validateOrderingAccount(sepipModel.getOrderingAccount());
		if (validateOrderingAccountStr != null) {
			validationResult += " ," + validateOrderingAccountStr;
		}

		String validateRfc = ValidationUtil.validateRfc(sepipModel.getRfcOrCurpPayer());
		if (validateRfc != null) {
			validationResult += " ," + validateRfc;
		}

		String validTimeFormat = ValidationUtil.validateTimeFormat(sepipModel.getCalenderTimePayment());
		if (validTimeFormat != null) {
			validationResult += " ," + validTimeFormat;
		}

		String dateCalenderDatePaymentStr = ValidationUtil
				.validateProperDate(Integer.toString(sepipModel.getCalenderDatePayment()), "ddmmyyyy");

		if (dateCalenderDatePaymentStr != null) {
			validationResult += " ," + dateCalenderDatePaymentStr;
		}

		String dateSpeiOperationDateStr = ValidationUtil
				.validateProperDate(Integer.toString(sepipModel.getSpeiOperationDate()), "ddmmyyyy");

		if (dateSpeiOperationDateStr != null) {
			validationResult += " ," + dateSpeiOperationDateStr;
		}

		String validateAmountOfPaymentStr = ValidationUtil.validateAmountOfPayment(sepipModel.getAmountOfPayment());

		if (validateAmountOfPaymentStr != null) {
			validationResult += " ," + validateAmountOfPaymentStr;
		}

		String validateConceptOfPaymentStr = ValidationUtil.validateConceptOfPayment(sepipModel.getConceptOfPayment());

		if (validateConceptOfPaymentStr != null) {
			validationResult += " ," + validateConceptOfPaymentStr;
		}

		String validateTypeOfBeneficiaryAccountStr = ValidationUtil
				.validateTypeOfBeneficiaryAccount(sepipModel.getTypeOfBeneficiaryAccount());

		if (validateTypeOfBeneficiaryAccountStr != null) {
			validationResult += " ," + validateTypeOfBeneficiaryAccountStr;
		}

		String validateCdaParticipientSPEIKeyStr = ValidationUtil
				.validateCdaParticipientSPEIKey(sepipModel.getCdaParticipientSPEIKey());

		if (validateCdaParticipientSPEIKeyStr != null) {
			validationResult += " ," + validateCdaParticipientSPEIKeyStr;
		}

		String validateNameOfTransferOrderIssuerParticipantStr = ValidationUtil
				.validateNameOfTransferOrderIssuerParticipant(sepipModel.getNameOfTransferOrderIssuerParticipant());

		if (validateNameOfTransferOrderIssuerParticipantStr != null) {
			validationResult += " ," + validateNameOfTransferOrderIssuerParticipantStr;
		}

		String validateOrderingNameStr = ValidationUtil.validateOrderingName(sepipModel.getOrderingName());

		if (validateOrderingNameStr != null) {
			validationResult += " ," + validateOrderingNameStr;
		}

		String validateOrdererAccountTypeStr = ValidationUtil
				.validateOrdererAccountType(sepipModel.getOrdererAccountType());

		if (validateOrdererAccountTypeStr != null) {
			validationResult += " ," + validateOrdererAccountTypeStr;
		}
		
		

		String validateTypeOfAccountOrd = ValidationUtil.typeOfAccountOrd(sepipModel.getOrdererAccountType());
		if (validateTypeOfAccountOrd != null) {
			validationResult += " ," + validateTypeOfAccountOrd;
		}
		
		String validateTypeOfAccountBene = ValidationUtil.typeOfAccountBene(sepipModel.getTypeOfBeneficiaryAccount());
		if (validateTypeOfAccountBene != null) {
			validationResult += " ," + validateTypeOfAccountBene;
		}
		
		String validateTypeOfAccountBene2 = ValidationUtil.typeOfAccountOrd(sepipModel.getBeneficiaryAccountType2());
		if (validateTypeOfAccountBene2 != null) {
			validationResult += " ," + validateTypeOfAccountBene2;
		} 
		
		String validationSPEIKey = ValidationUtil.validateSPEIKey(sepipModel.getCdaParticipientSPEIKey());
		if (validationSPEIKey != null) {
			validationResult += " ," + validationSPEIKey;
		} 
		
		/*String valitationFolioScheme = ValidationUtil.valitateFolioScheme(sepipModel.getPaymentType(), sepipModel.getDigitalCollectionSchemeFolio());
		if (valitationFolioScheme != null) {
			validationResult += " ," + valitationFolioScheme;
		} */
		
		String valitationPaymentOfFee = ValidationUtil.valitatePaymentOfFee(sepipModel.getPaymentType(), sepipModel.getPaymentOfTheTransferFee());
		if (valitationPaymentOfFee != null) {
			validationResult += " ," + valitationPaymentOfFee;
		}
		
		String valitationTransferFeeAmt = ValidationUtil.valitateTransferFeeAmt(sepipModel.getPaymentType(), sepipModel.getPaymentOfTheTransferFee(), sepipModel.getTransferFeeAmount() );
		if (valitationTransferFeeAmt != null) {
			validationResult += " ," + valitationTransferFeeAmt;
		} 
		
		String valitationCellAlias = ValidationUtil.valitateCellAlias(sepipModel.getPaymentType(), sepipModel.getBuyerCellNumberAliases() );
		if (valitationCellAlias != null) {
			validationResult += " ," + valitationCellAlias;
		} 
		
		String valitationDigitalBuyer = ValidationUtil.valitateDigitalBuyer(sepipModel.getPaymentType(), sepipModel.getDigitBuyersVerifier() );
		if (valitationDigitalBuyer != null) {
			validationResult += " ," + valitationDigitalBuyer;
		} 
		
		
		
		
		
		
		

		System.out.println("validationResult in Model" + validationResult);
		return validationResult;
	}

	public static String validateAllPaymentTypeInfo(int paymentType) {
		String validationResult = null;

		// Checking the Size of Payment Type field
		System.out.println("String.valueOf(paymentType).length() : " + String.valueOf(paymentType).length());
		if (String.valueOf(paymentType).length() == 0) {
			System.out.println("Length is less then 0");
			validationResult = "Tipo de Pago diferente a tercero a tercero, no puede estar vacío";
		}

		boolean validationPaymentTypeList = ValidationUtil.validatePaymentType(paymentType);

		if (!validationPaymentTypeList) {
			if (validationResult == null) {
				validationResult = "Tipo de Pago diferente a tercero a tercero";
			} else {
				validationResult += " , Tipo de Pago diferente a tercero a tercero";
			}
		}

		return validationResult;
	}

	public static String validateAllSpeiOperationDate(int speiOperationDate) {
		int length = String.valueOf(speiOperationDate).length();

		if (length == 0) {
			return "Fecha de operación de SPEI no puede estar vacía";
		}

		System.out
				.println("String.valueOf(speiOperationDate).length() : " + String.valueOf(speiOperationDate).length());

		if (length > 8) {
			return "Fecha inválida, mayor 8 dígitos";
		}

		if (length < 8) {
			return "Fecha inválida, menor 8 dígitos";
		}

		return null;
	}

	public static String validateBuyerCellNumberAliases(Long buyerCellNumberAliases) {
		int length = String.valueOf(buyerCellNumberAliases).length();

		if (length == 0) {
			return "El campo Alias del número celular del vendedor no puede estar vacío";
		}

		System.out.println(
				"String.valueOf(speiOperationDate).length() : " + String.valueOf(buyerCellNumberAliases).length());

		if (length > 10) {
			return "Valor inválido, el campo Alias del número celular del vendedor no debe ser mayor a 10 dígitos";
		}

		return null;
	}

	public static String validateOrderingAccount(Long orderingAccount) {
		int length = String.valueOf(orderingAccount).length();

		System.out.println("String.valueOf(orderingAccount).length() : " + String.valueOf(orderingAccount).length());

		if (length == 0) {
			return "El campo Cuenta Ordenante no puede no puede estar vacío";
		}

		if (length > 20) {
			return "Valor inválido, Cuenta Ordenante no puede ser mayor a 20 dígitos";
		}

		return null;
	}

	public static String validateBeneficiaryAccountType2(int beneficiaryAccountType2) {
		int length = String.valueOf(beneficiaryAccountType2).length();

		if (length == 0) {
			return "El campo Cuenta Beneficiario 2 no puede estar vacío";
		}

		System.out.println(
				"String.valueOf(speiOperationDate).length() : " + String.valueOf(beneficiaryAccountType2).length());

		if (length > 20) {
			return "Valor inválido, Cuenta Beneficiario 2 no puede ser mayor a 20 digitos";
		}

		if (length < 2) {
			return "Valor inválido, Tipo de Cuenta Beneficiario 2 no puede ser mayor a dos dígitos";
		}

		return null;
	}

	public static String validateDigitBuyersVerifier(int digitBuyersVerifier) {
		int length = String.valueOf(digitBuyersVerifier).length();

		System.out.println(
				"String.valueOf(speiOperationDate).length() : " + String.valueOf(digitBuyersVerifier).length());

		if (length > 3) {
			return "Valor inválido, el campo Dígitc Verificador del comprador  o debe ser mayor a 3 dígitos";
		}

		return null;
	}

	public static String validatePaymentOfTheTransferFee(int paymentOfTheTransferFee) {
		int length = String.valueOf(paymentOfTheTransferFee).length();

		System.out.println(
				"String.valueOf(speiOperationDate).length() : " + String.valueOf(paymentOfTheTransferFee).length());

		if (length == 0) {
			return "El campo Pago de la Comisión por la Transferencia no puede estar vacío";
		}

		if (length > 3) {
			return "Valor inválido para campo Pago de la Comisión por la Transferencia, el valor no puede ser mayor a 1 dígito.";
		}

		return null;
	}

	public static String validateCalenderTimePayment(String calenderTimePayment) {
		System.out.println("calenderTimePayment x.length() : " + calenderTimePayment.length());
		int length = String.valueOf(calenderTimePayment).length();

		if (length == 0) {
			return "El campo Hora del calendario no puede estar vacío";
		}

		Pattern specialCharacters = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
		Matcher match = specialCharacters.matcher(calenderTimePayment);
		boolean containsSpecialCharacters = match.find();

		if (containsSpecialCharacters) {
			return "Fecha de calendario del abono contiene caracteres especiales";
		}

		if (calenderTimePayment.length() > 6) {
			return "Hora inválida, mayor a 6 dígitos";
		}

		if (calenderTimePayment.length() < 6) {
			return "Hora inválida, menor a 6 dígitos";
		}

		return null;
	}

	public static boolean validatePaymentType(int paymentType) {
		System.out.println("paymentType : " + paymentType);
		boolean isValid = true;

		Integer[] arrayPaymnet = new Integer[] { 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 15, 16, 17, 18, 19, 20, 21, 22, 23,
				24, 25, 26, 27, 28, 29 };
		List<Integer> paymentList = Arrays.asList(arrayPaymnet);

		if (paymentList.contains(paymentType)) {
			System.out.println("It is valid ");
			return isValid;
		} else {
			System.out.println("Not valid ");
			return false;
		}
	}

	public static boolean validateProperDate(int dateValue) {
		String dateString = Integer.toString(dateValue);

		String date = dateString.substring(0, 1);
		String month = dateString.substring(3, 4);
		String year = dateString.substring(5, 8);

		return true;
	}

	/**
	 * Validates string of length() == 6 in hhmmss format
	 *
	 * @param timeToValidate Input time to be validated
	 * @return Returns true or false
	 */
	public static String validateTimeFormat(String timeToValidate) {
		if (timeToValidate == null)
			return "El campo tiempo no puede estar vacío";

		Pattern specialCharacters = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
		Matcher match = specialCharacters.matcher(timeToValidate);
		boolean containsSpecialCharacters = match.find();

		if (containsSpecialCharacters) {
			return "El campo tiempo contiene caracteres especiales";
		}

		if (timeToValidate.length() == 6) {
			Boolean validTime = Pattern.matches("^([0-1]\\d|2[0-3])([0-5]\\d)([0-5]\\d)$", timeToValidate);
			int hours = Integer.valueOf(timeToValidate.substring(0, 2));
			int minutes = Integer.valueOf(timeToValidate.substring(2, 4));
			int seconds = Integer.valueOf(timeToValidate.substring(4, 6));

			if (hours < 0 || hours > 24) {
				return "Las horas deben de ser menor a 24";
			}

			if (minutes < 0 || minutes > 59) {
				return "Los minutos deben de ser menor o igual a 59";
			}

			if (seconds < 0 || seconds > 59) {
				return "Los segundos deben de ser menor o igual a  59";
			}

			if (!validTime) {
				return "El formato del campo tiempo tiene un formato hhmmss no es válido";
			}
		} else {
			return "El formato del campo tiempo tiene un formato hhmmss no es válido";
		}
		return null;
	}

	/**
	 * Removes whitespace and validates RFC or CURP depending on string length
	 *
	 * @param rfcToValidate String to be validated
	 * @return Returns true or false
	 */
	public static String validateRfc(String rfcToValidate) {
		if (rfcToValidate == null)
			return "El campo RFC/CURP Ordenante no puede estar vacío";
		if (rfcToValidate.length() < 2 || rfcToValidate.length() > 18)
			return "El campo RFC/CURP Ordenante debe tener más de 2 caracteres y menos de 18";

		// Remove whitespace
		rfcToValidate = rfcToValidate.replaceAll("\\s+", "");

		// Validate RFC
		if (rfcToValidate.length() == 12 || rfcToValidate.length() == 13) {
			Boolean validRfc = Pattern.matches(
					"^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([A-Z]|[0-9]){2}([A]|[0-9]){1})?$",
					rfcToValidate);

			if (!validRfc) {
				return "El formato del RFC Ordenante no es válido";
			}
			// Validate CURP
		} else if (rfcToValidate.length() == 18) {
			Boolean validCurp = Pattern.matches(
					"/^([A-Z][AEIOUX][A-Z]{2}\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\\d])(\\d)$/",
					rfcToValidate);
			if (!validCurp) {
				return "El formato del CURP Ordenante no es válido";
			}
		} else {
			return "El formato del RFC/CURP Ordenante no es válido";
		}

		return null;
	}

	/**
	 * This method validates a string length
	 * 
	 * @param stringToValidate String to be validated
	 * @param minLength        Min allowed length of string
	 * @param maxLength        Max allowed length of string
	 * @param paymentType      Payment type, used for additional validations. If
	 *                         none enter 0
	 * @return Returns if string length is between range
	 */
	public static String validateInputLength(String stringToValidate, int minLength, int maxLength, int paymentType) {
		if (stringToValidate != null) {
			if (paymentType == 22
					&& (stringToValidate.compareTo("NA") == 0 || stringToValidate.compareTo("N/A") == 0)) {
				return "El campo Nombre Ordenante debe de ser 'NA'";
			}
			if (!(stringToValidate.length() >= minLength && stringToValidate.length() <= maxLength)) {
				return "El campo debe tener más de " + Integer.toString(minLength) + " caracteres y menos de "
						+ Integer.toString(minLength) + " caracteres";
			}
		}
		return "El campo Nombre Ordenante no puede estar vacío";
	}

	public static String validateProperDate(String dateToValidate, String dateFromat) {

		if (dateToValidate == null) {
			return "Fecha de operación del SPEI No puede estar vacío";
		}

		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);

		try {
			Date date = sdf.parse(dateToValidate);
		} catch (ParseException ps) {
			return "Fecha de operación del SPEI No puede estar vacío";
		}

		String day = dateToValidate.substring(0, 2);
		System.out.println("day : " + day);

		String month = dateToValidate.substring(2, 4);
		System.out.println("month : " + month);

		String year = dateToValidate.substring(4);
		System.out.println("year : " + year);

		if (year.length() < 4) {
			return "Fecha inválida, formato de año no válido";
		}

		if (Integer.parseInt(year) < 1000 || Integer.parseInt(year) > 3000) {
			return "Fecha inválida, año no válido";
		}

		if (Integer.parseInt(month) < 1 || Integer.parseInt(month) > 12) {
			return "Fecha inválida, mes incorrecto";
		}

		if (Integer.parseInt(month) == 2) {
			if (ValidationUtil.isLeapYear(Integer.parseInt(month))) {
				if (!(Integer.parseInt(day) != 29)) {
					return "Fecha inválida, día no válido";
				}
			} else {
				if (!(Integer.parseInt(day) != 28)) {
					return "Fecha inválida, día no válido";
				}
			}
		} else if (Integer.parseInt(month) == 1 || Integer.parseInt(month) == 3 || Integer.parseInt(month) == 5
				|| Integer.parseInt(month) == 7 || Integer.parseInt(month) == 8 || Integer.parseInt(month) == 10
				|| Integer.parseInt(month) == 12) {
			if (!(Integer.parseInt(day) != 31)) {
				return "Fecha inválida, día no válido";
			}
		} else if (Integer.parseInt(month) == 1 || Integer.parseInt(month) == 2 || Integer.parseInt(month) == 2
				|| Integer.parseInt(month) == 2 || Integer.parseInt(month) == 2) {
			if (!(Integer.parseInt(day) != 30)) {
				return "Fecha inválida, día no válido";
			}
		}

		return null;
	}

	public static boolean isLeapYear(int year) {

		if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
			return true;
		} else {
			return false;
		}
	}

	public static String validateAmountOfPayment(long amountOfPayment) {
		int count = 0;

		long length = String.valueOf(amountOfPayment).length();

		if (length < 1) {
			return "Monto Inválido, no puede ser 0.";
		}

		for (; amountOfPayment != 0; amountOfPayment /= 10, ++count)
			;

		System.out.println("Count : " + count);

		if (count > 12) {
			return "Monto Inválido, no puede ser  mayor a 999,999,999,999.99";
		}

		return null;
	}

	public static String validateConceptOfPayment(String conceptOfPayment) {

		if (conceptOfPayment.length() > 40) {
			return "Valor inválido, Concepto del Pago no puede ser mayor a 40 posiciones";
		}

		return null;

	}

	public static String validateOrderingName(String orderingName) {

		if (orderingName.length() > 40) {
			return "Valor inválido en el campo  Nombre Ordenante, la longitud máxima es de 40 posiciones.";
		}

		return null;

	}

	public static String validateTypeOfBeneficiaryAccount(int typeOfBeneficiaryAccount) {

		long length = String.valueOf(typeOfBeneficiaryAccount).length();

		if (length > 20) {
			return "Valor inválido, Cuenta Beneficiario no puede ser mayor a 20 digitos";

		}

		return null;

	}

	public static String validateCdaParticipientSPEIKey(int cdaParticipientSPEIKey) {

		long length = String.valueOf(cdaParticipientSPEIKey).length();

		if (length > 5) {
			return "Valor inválido, Clave SPEI del Participante Emisor de la CDA incorrecta  debe ser un valor numéricode longitud máxima de 5 digitos";

		}

		return null;

	}

	public static String validateNameOfTransferOrderIssuerParticipant(String nameOfTransferOrderIssuerParticipant) {
		if (nameOfTransferOrderIssuerParticipant.length() > 40) {
			return "Valor inválido en el campo  Nombre del Participante Emisor de la Orden de Transferencia, la longitud máxima es de 40 posiciones.";
		}

		return null;
	}

	public static String validateOrdererAccountType(int ordererAccountType) {

		long length = String.valueOf(ordererAccountType).length();

		if (length > 2) {
			return "Valor inválido, Tipo de Cuenta Ordenante no puede ser mayor a dos digitos";
		}

		return null;

	}
	
	public static String typeOfAccountOrd(int accountType) {
		Integer[] arrayAccounts = new Integer[] {40,3,4,5,6,7,8,9};
		List<Integer> accountList = Arrays.asList(arrayAccounts);
		int len = String.valueOf(accountType).length();
		
		if(!accountList.contains(accountType)) {
			return "Valor inválido, Tipo de Cuenta Ordenante  no está catalogado";
		}
		if(len > 2) {
			return "Valor inválido, Tipo de Cuenta Ordenante no puede ser mayor a dos digitos";
		}
		
		return null;
	}
	
	
	public static String typeOfAccountBene(int accountType) {
		Integer[] arrayAccounts = new Integer[] {40,3,4,5,6,7,8,9};
		List<Integer> accountList = Arrays.asList(arrayAccounts);
		int len = String.valueOf(accountType).length();
		
		if(!accountList.contains(accountType)) {
			return "Valor inválido, Tipo de Cuenta Beneficiario  no está catalogado";
		}
		if(len > 2) {
			return "Valor inválido, Tipo de Cuenta Beneficiario no puede ser mayor a dos digitos";
		}
		
		return null;
	}
	
	
	public static String typeOfAccountBene2(int accountType) {
		Integer[] arrayAccounts = new Integer[] {40,3,4,5,6,7,8,9};
		List<Integer> accountList = Arrays.asList(arrayAccounts);
		int len = String.valueOf(accountType).length();
		
		if(!accountList.contains(accountType)) {
			return "Valor inválido, Tipo de Cuenta Beneficiario 2 no está catalogado";
		}
		if(len > 2) {
			return "Valor inválido, Tipo de Cuenta Beneficiario 2 no puede ser mayor a dos digitos";
		}
		return null;
	}
	
	public static String validateSPEIKey(int cdaParticipientSPEIKey ) {
		int len = String.valueOf(cdaParticipientSPEIKey).length();
		if(len > 5) {
			return "Valor invalido, Clave SPEI del Participante Emisor de la CDA incorrecta debe ser un valor numericode longitud maxima de 5 digitos";
		}
		if (cdaParticipientSPEIKey != 40002) {
			return "Valor invalido, Clave SPEI del Participante Emisor de la CDA incorrecta  de acuerdo a catalogo.";
		}
		return null;
	}
	
	
	/*public static String valitateFolioScheme(int paymentType , String digitalFolio){
		Integer[] arrayPaymnet = new Integer[] {18,19,20,21,22};
		List<Integer> paymentList = Arrays.asList(arrayPaymnet);
		
		if (!paymentList.contains(paymentType)) {
			if(digitalFolio != "NA") {
				return "ERROR, add message for Folio";
			}
		}
		
		return null;
	}*/
		
	public static String valitatePaymentOfFee(int paymentType , int paymentOfFee){
		Integer[] arrayPaymnet = new Integer[] { 18,19,20,21,22};
		List<Integer> paymentList = Arrays.asList(arrayPaymnet);
		
		if (paymentList.contains(paymentType)) {
			int len = String.valueOf(paymentOfFee).length();
			if(len != 1) {
				return "ERROR, add message for Payment";
			}
		}else{
			if(paymentOfFee != 0) {
				return "Valor invalido para campo Pago de la comision por la transferencial, el valor debe ser 0";
			}
		}
		return null;
	}
	
	public static String valitateTransferFeeAmt(int paymentType , int paymentOfFee, double feeAmount){
		Integer[] arrayPaymnet = new Integer[] { 18,19,20,21,22};
		List<Integer> paymentList = Arrays.asList(arrayPaymnet);
		
		
		if (paymentList.contains(paymentType)) {
			if(paymentOfFee == 2) {
				if(feeAmount < 0){			
					return "Monto Invalido, no puede ser 0.";
		
				}else if(feeAmount > 999999999999.99){
					return "Monto Invalido, no puede ser  mayor a 999,999,999,999.99";
				}
			}
			else if(paymentOfFee == 1) {
				if(feeAmount != 0.00) {
					return "ERROR, error message does not exist yet";
				}
			}
		}else {			
			if(feeAmount != 0.00){
				return "ERROR, error message does not exist yet";
			}
		}
		
		return null;
	}

	public static String valitateCellAlias(int paymentType , String numAlias){
		Integer[] arrayPaymnet = new Integer[] {18,19,20,21,22};
		List<Integer> paymentList = Arrays.asList(arrayPaymnet);
		
		
		if (paymentList.contains(paymentType)) {
			if(numAlias != "NA" ){
				return "ERROR, error message does not exist yet";
			}
		}
		return null;
	}
	
	
	public static String valitateDigitalBuyer(int paymentType , String digBuyer){
		Integer[] arrayPaymnet = new Integer[] { 18,19,20,21,22};
		List<Integer> paymentList = Arrays.asList(arrayPaymnet);
		
		
		if (paymentList.contains(paymentType)) {
			if(digBuyer != "NA" ){
				return "ERROR, error message does not exist yet";
			}
		}
		return null;
	}

	

}
