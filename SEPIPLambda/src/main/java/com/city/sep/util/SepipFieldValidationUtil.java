/**
 * 
 */
package com.city.sep.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import com.city.sep.model.SepipModel;



/**
 * @author SU20053232
 *
 */
public class SepipFieldValidationUtil {
	
	public static boolean validateInputFields(SepipModel sepipModel) {
		
		boolean allValidationSuccessful = true;
		
		//boolean validateInputFieldLength = SepipFieldValidationUtil.validateInputFieldLength(sepipModel);
		
		boolean validationPaymentType = SepipFieldValidationUtil.validatePaymentType(sepipModel.getPaymentType());
		
		boolean speiKeyValidation = SepipFieldValidationUtil.validateCdaParticipientSPEIKey(sepipModel.getCdaParticipientSPEIKey());

		boolean dateValidationFechaDeOperacionDelSPEI = SepipFieldValidationUtil
				.validateDateFormat(Integer.toString(sepipModel.getSpeiOperationDate()), "ddmmyyyy");		

		boolean dateCalenderDatePayment = SepipFieldValidationUtil
				.validateDateFormat(Integer.toString(sepipModel.getCalenderDatePayment()), "ddmmyyyy");		
		
		boolean amtValidation = validateDoubleAmount(sepipModel.getAmountOfPayment());
		
		boolean ivaAmountValidation = validateDoubleAmount(sepipModel.getIvaAmount());
		boolean transferFeeAmountValidation = validateDoubleAmount(sepipModel.getTransferFeeAmount());

		boolean rfcValidation = validateRfc(sepipModel.getRfcOrCurpPayer());
		boolean validOrderingName = validateInputLength(sepipModel.getOrderingName(), 0, 40, sepipModel.getPaymentType());

		boolean timeValidation = validateTimeFormat(sepipModel.getCalenderTimePayment());

		if (!dateValidationFechaDeOperacionDelSPEI || !validationPaymentType || !dateCalenderDatePayment || !amtValidation
				|| !rfcValidation || !timeValidation || !ivaAmountValidation || !transferFeeAmountValidation || !validOrderingName) {
			return false;
		}
		
		return allValidationSuccessful;
	}
	
	public static boolean validateDateFormat(String dateToValidate, String dateFormat) {
		if(dateToValidate == null) {
			return false;
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);
		
		try {
			Date date = sdf.parse(dateToValidate);	
			System.out.println(date);
		} catch(ParseException ps) {
			ps.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static boolean validateAmount(int amount) {
		boolean isValidAmount = true;
		
		if (amount <=0 || amount >  999999999999.99) {
			return false;
		}
		
		return isValidAmount;
	}
	
	public static boolean validateDoubleAmount(double amount) {
		boolean isValidAmount = true;
		
		if (amount <=0 || amount >  999999999999.99) {
			return false;
		}
		
		return isValidAmount;
	}

	/**
	 * This method validates a Payment Type
	 * @param paymentType Payment type to be validated, ex: 1, 2...
	 * @return Returns true or false depending on the validation
	 */
	public static boolean validatePaymentType(int paymentType) {
		System.out.println(paymentType);
		boolean isValid = true;
		
		Integer [] arrayPayment = new Integer[]{1,3,4,5,6,8,9,10,11,12,15,25,26,27,28};
		List<Integer> paymentList = Arrays.asList(arrayPayment);
		
		if (paymentList.contains(paymentType)) {
		    return isValid;		    
		} else {
			return false;
		}
	}

	public static boolean validateCdaParticipientSPEIKey(int cdaParticipientSPEIKey) {
		System.out.println(cdaParticipientSPEIKey);
		boolean isValid = true;

		Integer [] cdaParticipientSPEIKeyValues = new Integer[] { 40002,40012,40014,40019,40021,40030,40032,40036,40037,40042,40044,40058,40059,40062,40072,40102,40103,40106,40132 };
		List<Integer> paymentList = Arrays.asList(cdaParticipientSPEIKeyValues);

		if (paymentList.contains(cdaParticipientSPEIKey)) {
		    return isValid;
		} else {
			return false;
		}
	}

	/**
	 * Validates string of length() == 6 in hhmmss format
	 * @param timeToValidate Input time to be validated
	 * @return Returns true or false
	 */
	public static boolean validateTimeFormat(String timeToValidate) {
		if (timeToValidate == null) return false;

		if (timeToValidate.length() == 6) {
			return Pattern.matches("^([0-1]\\d|2[0-3])([0-5]\\d)([0-5]\\d)$", timeToValidate);
		}

		return true;
	}

	/**
	 * Removes whitespace and validates RFC or CURP depending on string length
	 * @param rfcToValidate String to be validated
	 * @return Returns true or false
	 */
	public static boolean validateRfc(String rfcToValidate) {
		if (rfcToValidate == null) return false;
		if (rfcToValidate.length() == 0 || rfcToValidate.length() > 18) return false;

		// Remove whitespace
		rfcToValidate = rfcToValidate.replaceAll("\\s+", "");

		// Validate RFC
		if (rfcToValidate.length() == 12 || rfcToValidate.length() == 13) {
			return Pattern.matches("^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([A-Z]|[0-9]){2}([A]|[0-9]){1})?$", rfcToValidate);
		}

		// Validate CURP
		if (rfcToValidate.length() == 18) {
			return Pattern.matches("/^([A-Z][AEIOUX][A-Z]{2}\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\\d])(\\d)$/", rfcToValidate);
		}

		return true;
	}

	/**
	 *
	 * @param stringToValidate String to be validated
	 * @param minLength Min allowed length of string
	 * @param maxLength Max allowed length of string
	 * @param paymentType Payment type, used for additional validations. If none enter 0
	 * @return Returns if string length is between range
	 */
	public static boolean validateInputLength(String stringToValidate, int minLength, int maxLength, int paymentType) {
		if (stringToValidate != null) {
			if (paymentType == 04 && (stringToValidate.compareTo("NA") == 0 || stringToValidate.compareTo("N/A") == 0)) {
				return true;
			}
			return stringToValidate.length() >= minLength && stringToValidate.length() <= maxLength;
		}
		return false;
	}
}
